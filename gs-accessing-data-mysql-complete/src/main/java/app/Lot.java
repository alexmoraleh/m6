package app;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="Lot")
public class Lot {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	int id;
	@Column
	int quantitat;
	@Column
	Date dataEntrada = new Date();
	@Column
	Date dataCaducitat= null;

	//Relacion n a 1 con producto
	@ManyToOne
	@JoinColumn(name="producte")
	Producte producte;


	@SuppressWarnings("deprecation")
	Lot(){
		Random rnd=new Random();
		quantitat=rnd.nextInt(500);
		dataEntrada=new Date();
		dataCaducitat=new Date(rnd.nextInt(2020)+2018, rnd.nextInt(12)+1, rnd.nextInt(30)+1);
	}
	Lot(Producte p){
		this();
		producte=p;
	}
	//sjkdkleslkjlisje
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getQuantitat() {
		return quantitat;
	}

	public void setQuantitat(int quantitat) {
		this.quantitat = quantitat;
	}

	public Date getDataEntrada() {
		return dataEntrada;
	}

	public void setDataEntrada(Date dataEntrada) {
		this.dataEntrada = dataEntrada;
	}

	public Date getDataCaducitat() {
		return dataCaducitat;
	}

	public void setDataCaducitat(Date dataCaducitat) {
		this.dataCaducitat = dataCaducitat;
	}
	public Producte getProductes() {
		return producte;
	}
	public void setProductes(Producte productes) {
		this.producte = productes;
	}
	
	
	
}
