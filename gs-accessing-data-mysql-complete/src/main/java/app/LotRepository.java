package app;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

public interface LotRepository extends CrudRepository<Lot, Integer> {

	List<Lot> findByProducte(int id);
}