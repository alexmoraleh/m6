package app;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

@Entity
@Table(name="Client")
public class Client {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "CIF")
	protected int CIF;
	
	@Column(name = "nomClient")
	protected String nomClient;
	
	@Column
	protected boolean actiu;
	
	@OneToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name="adreca", nullable=false)
	protected Address dir;

	//relacio 1 a n amb comanda
	//relacio 1 a 1 amb adreça
	
	@OneToMany
	@JoinColumn(name="Client")
	protected Set <Comanda> comandas=new HashSet<Comanda>();
	
	public int getCIF() {
		return CIF;
	}

	public void setCIF(int cIF) {
		CIF = cIF;
	}

	public String getNomClient() {
		return nomClient;
	}

	public void setNomClient(String nomClient) {
		this.nomClient = nomClient;
	}

	public boolean isActiu() {
		return actiu;
	}

	public void setActiu(boolean actiu) {
		this.actiu = actiu;
	}
		
}
