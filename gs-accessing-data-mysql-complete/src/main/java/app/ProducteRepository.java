package app;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

public interface ProducteRepository extends CrudRepository<Producte, Integer> {
	
	Iterable<Producte> findAllBynomProducte(String nom);
	List<Producte> findByNomProducteOrderByPreuVendaDesc(String nomProducte);
	//List<Profesor> findByApe1OrderByNombreDesc(String ape1);
}
