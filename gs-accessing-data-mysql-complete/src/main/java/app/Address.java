package app;

import java.util.Random;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="Adreca")
public class Address {
	@Column
	protected boolean principal;
	@Column
	protected String carrer;
	@Column
	protected int numero;
	@Column
	protected String poblacio;
	@Column
	protected String pais;
	@Column
	protected String postal;
	@Column
	protected String telefon;
	@Id
	@Column
	protected double latitud;
	@Column
	protected double longitud;
	
	/*@OneToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name="client", nullable=false)
	protected Client client;*/
	//relacio 1 a 1 amb client

}
