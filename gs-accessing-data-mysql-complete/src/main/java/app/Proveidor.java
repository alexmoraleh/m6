package app;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import javax.persistence.*;

@Entity
@Table(name = "Proveidor")
public class Proveidor{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(updatable = false, nullable = false)
	protected int idProveidor;
	
	@Column
	protected String nomProveidor;
	
	@Column
	protected String CIF;
	
	@Column
	protected boolean actiu;
	
	@Column
	protected String personaContacte;
	
	//relacio 1 a 1 amb address
	@OneToOne(cascade= {CascadeType.PERSIST})
	@JoinColumn(name="numero")
	protected Address numero;
	
	//relacio 1 a n amb peticioproveidor
	@JoinColumn(name="PeticioProveidor")
	@OneToMany(cascade = {CascadeType.REFRESH})
	private Set<PeticioProveidor> peticions=new HashSet<PeticioProveidor>();
}