package app;

import java.util.Date;
import java.util.Random;

import javax.persistence.*;

@Entity
@Table(name = "PeticioProveidor")
public class PeticioProveidor {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	protected int idPeticio;
	
	@JoinColumn(name="Comanda")
	@ManyToOne(cascade = {CascadeType.REFRESH})
	protected Comanda comanda;
	
	@Column
	protected int tipusPeticio;
	
	@Column
	protected Date dataOrdre;
	
	@Column
	protected Date dataRecepcio;   
	
	@Column
	protected ComandaEstat estat;
	
	//relacio n a 1 amb proveidor
	@ManyToOne(cascade = {CascadeType.REFRESH})
	@JoinColumn(name="Proveidor")
	protected Proveidor proveidor;
	
	//relacio n a 1 amb producte
	@ManyToOne(cascade = {CascadeType.REFRESH})
	@JoinColumn(name="Producte")
	protected Producte producte;
	
	/* xd */
	PeticioProveidor() {
		Random rnd=new Random();
		// TODO Auto-generated constructor stub
		tipusPeticio=rnd.nextInt(50);
		dataOrdre=new Date(rnd.nextInt(2017)+2016, rnd.nextInt(12)+1, rnd.nextInt(30)+1);
		dataRecepcio= new Date();
		estat=ComandaEstat.values()[rnd.nextInt(4)];
	}
	PeticioProveidor(Comanda c, Proveidor p, Producte pr) {
		this();
		comanda=c;
		proveidor=p;
		producte=pr;
	}
}
