import pymongo
import datetime
import random
from pymongo import MongoClient
#python -m pip install pymongo

client = MongoClient()
db = client["project"]
coll = db["pokemon"]
res = None
# Al saber que no hay switch en mongo he tirado por el clasico if else, luego he caido en las
# funciones 🤦‍. Igual se me ha ido un poco de las manos <3
#xd
while True:
    consulta = input()
    if consulta == "": break
    con = consulta.split(" ")
    if str.isdigit(con[1]):
        res = coll.find_one({"num": con[1]})
    if con[0] == "Type" or con[0] == "type":
        res = coll.find({"type": {"$in": [con[1]]}})
        if res is not None:
            tupla = []
            for r in res:
                tupla.append(r["name"])
            print(tupla, coll.count_documents({"type": {"$in": [con[1]]}}))
        else:
            print("El pokemon no existe")
    else:
        if res is None:
            res = coll.find_one({"name": con[1]})
        if con[len(con)-1] == "evol" or con[len(con)-1] == "Evol":
            arr = []
            e = 0
            if "prev_evolution" in res:
                while e < len(res["prev_evolution"]):
                    arr.append(res["prev_evolution"][e]["name"])
                    e += 1
            e = 0
            arr.append(res["name"])
            if "next_evolution" in res:
                while e < len(res["next_evolution"]):
                    arr.append(res["next_evolution"][e]["name"])
                    e += 1
            print(arr)
        elif con[0] == "Search" or con[0] == "search":
            if res is not None:
                tupla = (res["name"],)
                for i in range(2, len(con)):
                    tupla += (res[con[i]],)
                print(tupla)
            else:
                print("El pokemon no existe")
        elif con[0] == "Catch" or con[0] == "catch":
            if res is not None:
                coll = db["team"]
                aux = None
                if str.isdigit(con[1]):
                    aux = coll.find_one({"num": con[1]})
                else:
                    aux = coll.find_one({"name": con[1]})
                if aux is None:
                    insert = {
                        "_id": res["_id"],
                        "num": res["num"],
                        "name": res["name"],
                        "catch_date": datetime.datetime.now().isoformat(),
                        "CP": random.randint(0, 500),
                        "candy": res["candy"],
                        "current_candy": 0
                    }
                    if "prev_evolution" in res:
                        insert["prev_evolution"] = res["prev_evolution"]
                    if "next_evolution" in res:
                        insert["next_evolution"] = res["next_evolution"]
                        insert["candy_count"] = res["candy_count"]
                    coll.insert_one(insert)
                    # cuantos=coll.find({"num":res["num"]})
                    print(res["name"], "atrapat. Nombre de Pokémon: ", coll.find(
                        {"num": res["num"]}).collection.estimated_document_count())
                else:
                    print("Ja tens aquest pokemon")
            else:  # algo he roto que ni el search funciona
                print("El pokemon no existe")
        elif con[0] == "Release" or con[0] == "release":
            coll = db["team"]
            if coll.find_one({"num": res["num"]}) is not None:
                coll.delete_one({"num": res["num"]})
                print(res["name"], "alliberat. Nombre de Pokémon: ", coll.find(
                    {"num": res["num"]}).collection.estimated_document_count())
            else:
                print("No tens aquest pokemon")
        elif con[0] == "Candy" or con[0] == "candy":
            coll = db["team"]
            team = coll.find_one({"num": res["num"]})
            if team is not None:
                if team["current_candy"] < team["candy_count"]-1:
                    coll.update_one({"num": team["num"]}, {
                                    "$set": {"current_candy": team["current_candy"]+1}})
                    print("Caramel donat a ", team["name"] + ". Caramels ",
                        team["current_candy"], "/", team["candy_count"])
                else:
                    col = db["pokemon"]
                    nombre = res["name"]
                    fecha=res["catch_date"]
                    res = col.find_one({"num": team["next_evolution"][0]["num"]})
                    insert = {
                        "_id": res["_id"],
                        "num": res["num"],
                        "name": res["name"],
                        "catch_date": fecha,
                        ##arreglar arriba fecha
                        "CP": team["CP"]+100,
                        "candy": res["candy"],
                        "current_candy": 0
                    }
                    if "prev_evolution" in res:
                        insert["prev_evolution"] = res["prev_evolution"]
                    if "next_evolution" in res:
                        insert["next_evolution"] = res["next_evolution"]
                        insert["candy_count"] = res["candy_count"]
                    coll.insert_one(insert)
                    delete = {"name": nombre}
                    coll.delete_one(delete)
                    print(nombre, " evoluciona a ", res["name"], "!")
            else:
                print("No tens aquest pokemon")
