import pymongo
from pymongo import MongoClient
from mongoengine import *
import os
import random
 
 
client = MongoClient()
db = client["project"]
item = db["item"]
teamA = db["TeamA"]
teamB = db["TeamB"]
 
 
coll = db["teama"]
teama = Team(name='TeamA', pokemons=coll.find({},{"_id":0,"name":1}))
coll = db["teamb"]
teamb = Team(name='TeamB', pokemons=coll.find({},{"_id":0,"name":1}))
 
combat()
 
 
# PARTE 2
#class.objects[0]
#hola = class
#hola.atributo
#hola.objects.count()
#premium = StringField(choices=SINO) esto es la def de un atributo de una class, SINO es una tupla ('Si','No')
#author = ReferenceField(User, reverse_delete_rule=CASCADE) por si acaso
 
#if isinstance(post, LinkPost): esto es un instanceof
#        print('Link: {}'.format(post.link_url)) esto no se que mierdas hace
#num_posts = Post.objects(tags='mongodb').count() puede ser util
 
 
#MOVES
class FastMoves(Move):
    Type = StringField(required = True)
    energyGain = IntField(required = True, min_value = 0, max_value = 20)
 
class ChargedMoves(Move):
    Type = StringField(required = True)
    energyCost = IntField(required = True, min_value = 33, max_value = 100)
 
class Moves(Document):
    name = StringField(required = True)
    pwr = IntField(required = True, min_value = 0, max_value = 180)
    Type = StringField(required = True)
    meta = {'allow_inheritance': True}
 
#POKEMON
 
TYPES = ("Normal", "Fire", "Water", "Grass", "Electric", "Ice", "Fighting", "Poison", "Ground", "Flying", "Psychic", "Bug", "Rock", "Ghost", "Dark", "Dragon", "Steel", "Fairy")
 
class Evolutions(EmbeddedDocument):
    name = StringField(required = True)
    num = IntField(required = True)
 
 
class Pokemon(Document):
    _id = IntField(required = True)
    name = StringField(required = True)
    num = IntField(required = True)
    img = StringField()
    type = ListField(choices=TYPES)
    height = DecimalField(required = True)
    weight = DecimalField(required = True)
    candy = StringField(required = True)
    candy_count = IntField(required = True)
    egg = StringField(required = True)
    spawn_chance = DecimalField(required = True)
    avg_spawns = DecimalField(required = True)
    spawn_time = StringField(required = True)
    multipliers = DecimalField(required = True)
    weaknesses = ListField(choices = TYPES)
    next_evolution = ListField(EmbeddedDocumentField(Evolutions))
    prev_evolution = ListField(EmbeddedDocumentField(Evolutions))
 
#TEAMS
class Team():
    name = StringField(required = True)
    pokemons = LisField(StringField(max_lenght=30))
   
class TeamA(Document):
    _id = IntField(required = True)
    name = StringField(required = True)
    num = IntField(required = True)
    img = StringField()
    type = ListField(choices=TYPES)
    atk = IntField(required = True)
    defe = IntField(required = True)
    energy = IntField(required = True)
    moves = ListField(EmbeddedDocument(Moves))
    candy = StringField(required = True)
    candy_count = IntField(required = True)
    current_candy = IntField(required = True)
    weaknesses = ListField(choices = TYPES)
    next_evolution = ListField(EmbeddedDocumentField(Evolutions))
    prev_evolution = ListField(EmbeddedDocumentField(Evolutions))
 
class TeamB(Document):
    _id = IntField(required = True)
    name = StringField(required = True)
    num = IntField(required = True)
    img = StringField()
    type = ListField(choices=TYPES)
    atk = IntField(required = True)
    defe = IntField(required = True)
    energy = IntField(required = True)
    moves = ListField(EmbeddedDocument(Moves))
    candy = StringField(required = True)
    candy_count = IntField(required = True)
    current_candy = IntField(required = True)
    weaknesses = ListField(choices = TYPES)
    next_evolution = ListField(EmbeddedDocumentField(Evolutions))
    prev_evolution = ListField(EmbeddedDocumentField(Evolutions))
 
#ITEMS
class Item(Post):
    name = StringField(required = True)
    Type = StringField(required = True)
    value = IntField(required = True)
 
 
#FUNCTIONS
 
def Prepare():
    i = 0
    while i < 6 :
        PokeMonA = Pokemon.objects[random.randint(1,151)]
        PokeA = new TeamA()
        PoKeMonB = Pokemon.objects[random.randint(1, 151)]
        PokeB = new TeamB()
        PokeB.save()
        PokeA.save()
        i += 1
       
   
 
   
 
def Attack(PokemonAttacking, PokemonDefending):
    acumulador = 1
    mvnpwr = 0
    atkpwr = PokemonAttacking["atk"]
    defdef = PokemonDefending["def"]
    tipo = input()
    if tipo == "Attack Fast":
        pass
    elif tipo == "Attack Charged":
        pass
   
    AtkType = PokemonAttacking["type"]
    DefWeak = PokemonDefending["weaknesses"]
    DefType = PokemonDefending["type"]
    extra = 0
    for tatk in AtkType:
        for wdef in DefWeak:
            if tatk == wdef:
                acumulador = 2
       
        for tdef in DefType:
            if tatk == tdef:
                extra = 0.5
   
    acumulador = acumulador + extra
 
    PokemonDefending["HP"] = PokemonDefending["HP"] - (((atkpwr * mvnpwr) * acumulador)/(defdef*2))
    return PokemonDefending
 
def Use(consulta, PokemonTarget):
    usingItem = Item.objects.get(name=consulta)[0]
   
    if usingItem["type"] == "health":
        PokemonTarget["HP"] += usingItem["value"]
        if PokemonTarget["HP"] > PokemonTarget["HPmax"]:
            PokemonTarget["HP"] = PokemonTarget["HPmax"]
    elif usingItem["type"] == "AttackX":
        PokemonTarget["atk"] += usingItem["value"]
    else:
        PokemonTarget["def"] += usingItem["value"]
 
    return PokemonTarget
 
def Change(consulta, turn):
    pokemonName = consulta.split(" ")
 
    if turn == 0:
        ChangePokemon = TeamA.objects.get(name=pokemonName)[0]
    else:
        ChangePokemon = TeamB.objects.get(name=pokemonName)[0]
   
    return ChangePokemon
 
def Combat():
    #se inicia el combate
    Prepare()
    pokemonA = TeamA.objects[0]
    pokemonB = TeamB.objects[0]
    countA = TeamA.objects.count()
    countB = TeamB.objects.count()
    turn = 0
 
    while (countA > 0) and (countB > 0):
        consulta = input()
        if "Attack" in consulta:
            if turn == 0:
                pokemonB = Attack(pokemonA, pokemonB)
                turn = 1
            else:
                pokemonA = Attack(pokemonB, pokemonA)
                turn = 0
 
        elif "Use" in consulta:
            if turn == 0:
                pokemonA = Use(consulta.split(" "), pokemonA)
                turn = 1
            else:
                pokemonB = Use(consulta.split(" "), pokemonB)
                turn = 0
       
        elif "Change" in consulta:
            if turn != 1:
                pokemonA = Change(consulta, turn)
                turn = 1
            else:
                pokemonB = Change(consulta, turn)
                turn = 0
       
        if pokemonA["HP"] <= 0:
            print("Pokemon del Equipo A " + pokemonA["name"] + " ha muerto. Se procede a sacar el siguente pokemon.")
            pokemonA.delete()
            pokemonA = TeamA.objects[0]
        if pokemonB["HP"] <= 0:
            print("Pokemon del Equipo B " + pokemonB["name"] + " ha muerto. Se procede a sacar el siguente pokemon.")
            pokemonA.delete()
            pokemonB = TeamB.objects[0]
 
        countA = TeamA.objects.count()
        countB = TeamB.objects.count()
 
        print("Pokemon A: " + pokemonA["name"] + ", HP:"+ pokemonA["HP"] + ", Energia: "+ pokemonA["energy"])
        print("Pokemon B: " + pokemonB["name"] + ", HP:"+ pokemonB["HP"] + ", Energia: "+ pokemonB["energy"])
    if countA > countB:
        print("Team A wins! Team B losses 40milion dollars.")
    else:
        print("Team B wins! Team A losses 40milion dollars.")