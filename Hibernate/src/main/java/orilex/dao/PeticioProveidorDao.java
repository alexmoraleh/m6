package orilex.dao;

import java.util.Date;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import orilex.Hivernate.Comanda;
import orilex.Hivernate.Lot;
import orilex.Hivernate.PeticioProveidor;

public class PeticioProveidorDao extends GenericDao<PeticioProveidor, Integer> implements lPeticioProveidorDao{

	@Override
	public void delete(PeticioProveidor id) {
			Session session = sessionFactory.getCurrentSession();
			try {
				session.beginTransaction();
				PeticioProveidor entity = (PeticioProveidor) session.get(getEntityClass(), id.getIdPeticio());
				session.delete(entity);
				session.getTransaction().commit();
			} catch (HibernateException e) {
				e.printStackTrace();
				if (session != null && session.getTransaction() != null) {
					System.out.println("\n.......Transaction Is Being Rolled Back.......");
					session.getTransaction().rollback();
				}
				e.printStackTrace();

			}
		}
	}
