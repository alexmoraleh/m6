package orilex.dao;

import java.util.Date;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import orilex.Hivernate.Lot;

public class LotDao extends GenericDao<Lot, Integer> implements lLotDao{

	@Override
	public void delete(Lot id) {
		if(id.getQuantitat() > 0 && id.getDataCaducitat().compareTo(new Date()) > 0) {
			System.out.println("Encara queden productes en el lot y no ha caducat.");
		} else {
			Session session = sessionFactory.getCurrentSession();
			try {
				session.beginTransaction();
				Lot entity = (Lot) session.get(getEntityClass(), id.getId());
				session.delete(entity);
				session.getTransaction().commit();
			} catch (HibernateException e) {
				e.printStackTrace();
				if (session != null && session.getTransaction() != null) {
					System.out.println("\n.......Transaction Is Being Rolled Back.......");
					session.getTransaction().rollback();
				}
				e.printStackTrace();

			}
		}
	}
	
	@Override
	public void saveOrUpdate(Lot c) {
		if(c.getDataCaducitat().compareTo(new Date()) <= 0) {
			System.out.println("El lot ha expirat. No pot actualitzarlo o afeguirlo.");
		} else {
			Session session = sessionFactory.getCurrentSession();
			try {
				session.beginTransaction();
				session.saveOrUpdate(c);
				session.getTransaction().commit();

			} catch (HibernateException e) {
				e.printStackTrace();
				if (session != null && session.getTransaction() != null) {
					System.out.println("\n.......Transaction Is Being Rolled Back.......");
					session.getTransaction().rollback();
				}
				e.printStackTrace();

			}
		}
	}
}
