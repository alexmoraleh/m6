package orilex.dao;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.event.spi.LoadEvent;
import org.hibernate.event.spi.LoadEventListener;
import org.hibernate.event.spi.LoadEventListener.LoadType;


public class LoadEventListenerImp implements LoadEventListener {
	   
	   private static final long serialVersionUID = 1L;
	   private static Logger logger = LogManager.getLogger(LoadEventListenerImp.class);

	   @Override
	   public void onLoad(LoadEvent e, LoadType type) throws HibernateException {
	      logger.info("onLoad is called. 2");
	      Object obj = e.getResult();
	   }

	}
