package orilex.dao;
import java.util.List;

import orilex.Hivernate.Client;

public interface lClientDao extends lGenericDao<Client, Integer>{
	
	void saveOrUpdate(Client c);

	Client get(Integer id);

	List<Client> list();

	void delete(Integer id);
}
