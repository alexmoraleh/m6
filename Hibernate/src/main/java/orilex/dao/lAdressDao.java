package orilex.dao;

import java.util.List;

import orilex.Hivernate.Address;

public interface lAdressDao extends lGenericDao<Address,Integer>{

	void saveOrUpdate(Address m);

	Address get(Integer id);

	List<Address> list();

	void delete(Integer id);
}
