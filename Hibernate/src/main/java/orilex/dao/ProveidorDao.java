package orilex.dao;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import orilex.Hivernate.Proveidor;

public class ProveidorDao extends GenericDao<Proveidor, Integer> implements lProveidorDao{

	@Override
	public void delete(Proveidor id) {
		Session session = sessionFactory.getCurrentSession();
		try {
			session.beginTransaction();
			Proveidor entity = (Proveidor) session.get(getEntityClass(), id.getIdProveidor());
			session.delete(entity);
			session.getTransaction().commit();
		} catch (HibernateException e) {
			e.printStackTrace();
			if (session != null && session.getTransaction() != null) {
				System.out.println("\n.......Transaction Is Being Rolled Back.......");
				session.getTransaction().rollback();
			}
			e.printStackTrace();

		}
	}
	
}
