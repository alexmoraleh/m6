package orilex.dao;

import java.util.List;

import orilex.Hivernate.Client;
import orilex.Hivernate.Comanda;

public interface lComandaDao extends lGenericDao <Comanda, Integer>{
	
	void saveOrUpdate(Comanda c);

	Comanda get(Integer id);

	List<Comanda> list();

	void delete(Comanda id);
}
