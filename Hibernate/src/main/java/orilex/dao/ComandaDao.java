package orilex.dao;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import orilex.Hivernate.*;

public class ComandaDao extends GenericDao<Comanda, Integer> implements lComandaDao{

	public void delete(Comanda id) {
		// TODO Auto-generated method stub
		if(id.getEstat() != ComandaEstat.LLIURAT) {
			System.out.println("La no ha sigut lliurada no es pot borrar");
		} else {
			Session session = sessionFactory.getCurrentSession();
			try {
				session.beginTransaction();
				Comanda entity = (Comanda) session.get(getEntityClass(), id.getIdComanda());
				session.delete(entity);
				session.getTransaction().commit();
			} catch (HibernateException e) {
				e.printStackTrace();
				if (session != null && session.getTransaction() != null) {
					System.out.println("\n.......Transaction Is Being Rolled Back.......");
					session.getTransaction().rollback();
				}
				e.printStackTrace();

			}// FALTA SABER COMO SE BORRA CON LA COMANDA DIRECTAMENTE SIN PASAR POR ENTITYCLASS	*/
		}
	}

	
}
