package orilex.dao;

import java.util.List;


import orilex.Hivernate.Proveidor;

public interface lProveidorDao extends lGenericDao<Proveidor, Integer>{
	void saveOrUpdate(Proveidor c);

	Proveidor get(Integer id);

	List<Proveidor> list();

	void delete(Proveidor id);
}
