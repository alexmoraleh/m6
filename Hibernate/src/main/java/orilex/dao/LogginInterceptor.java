package orilex.dao;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.Set;

import orilex.Hivernate.*;

import org.hibernate.EmptyInterceptor;
import org.hibernate.type.Type;

public class LogginInterceptor extends EmptyInterceptor {
	static Random rnd = new Random();
	private static final long serialVersionUID = 1L;
	// Define a static logger
	// private static Logger logger =
	// LogManager.getLogger(LoggingInterceptor.class);

	@Override
	public boolean onSave(Object entity, Serializable id, Object[] state, String[] propertyNames, Type[] types) {
		System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
		System.out.println("||onSave method is called. ||");
		System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
		
		System.out.println(entity.toString());
		
		if (entity instanceof Producte) {
			System.out.println("Alla vamos jiuston");
			Producte p = (Producte) entity;
			if(p.getStock() <= p.getStockMinim()) {
				System.out.println("El producte amb ID: "+ p.getCodiProducte() + ", esta per sota del stock minim!!\n"
						+ "STOCK ACTUAL: " + p.getStock() + "\n"
						+ "STOCK MINIM: " + p.getStockMinim() + "\n");
			}
			ProveidorDao pd = new ProveidorDao();
			boolean proveedor = false;
			Set<PeticioProveidor> spp = p.getPeticio();
			Proveidor pr = null;
			for(PeticioProveidor pp: spp) {
				if(pp.getProducte().getCodiProducte() == p.getCodiProducte()) {
					pr = pp.getProveidor();
					proveedor = true;
					break;
				}
			}
			if(!proveedor) {
				pr = pd.list().get(rnd.nextInt(0)+(pd.list().size()-1));
			}
			
			PeticioProveidor pdr = new PeticioProveidor();
			pdr.setProveidor(pr);
			pdr.setDataOrdre(new Date());
			pdr.setDataRecepcio(new Date());
			pdr.setEstat(ComandaEstat.PENDENT);
			pdr.setProducte(p);
			pdr.setTipusPeticio(rnd.nextInt(0)+3);
			
			PeticioProveidorDao ppd = new PeticioProveidorDao();
			ppd.saveOrUpdate(pdr);
			
		}
		
		return super.onSave(entity, id, state, propertyNames, types);

	}

	@Override
	public String onPrepareStatement(String sql) {
		// logger.info(sql);
		return super.onPrepareStatement(sql);
	}
}
