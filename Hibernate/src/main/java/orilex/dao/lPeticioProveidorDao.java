package orilex.dao;

import java.util.List;

import orilex.Hivernate.Client;
import orilex.Hivernate.Comanda;
import orilex.Hivernate.PeticioProveidor;

public interface lPeticioProveidorDao extends lGenericDao<PeticioProveidor, Integer>{
	void saveOrUpdate(PeticioProveidor c);

	PeticioProveidor get(Integer id);

	List<PeticioProveidor> list();

	void delete(PeticioProveidor id);
}
