package orilex.dao;

import java.util.List;

import orilex.Hivernate.Lot;

public interface lLotDao extends lGenericDao<Lot, Integer>{
	
	void saveOrUpdate(Lot c);

	Lot get(Integer id);

	List<Lot> list();

	void delete(Lot id);
}
