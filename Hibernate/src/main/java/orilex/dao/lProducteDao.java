package orilex.dao;

import java.util.List;

import orilex.Hivernate.Producte;

public interface lProducteDao extends lGenericDao<Producte, Integer>{
	void saveOrUpdate(Producte c);

	Producte get(Integer id);

	List<Producte> list();

	void delete(Producte id);
}
