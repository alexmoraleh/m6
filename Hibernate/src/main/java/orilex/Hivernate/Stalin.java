package orilex.Hivernate;

import java.util.ArrayList;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;


public class Stalin {
	static Session session;
	static SessionFactory sessionFactory;
	static ServiceRegistry serviceRegistry;

	public static synchronized SessionFactory getSessionFactory() {
	    if ( sessionFactory == null ) {

	        // exception handling omitted for brevityaa

	        serviceRegistry = new StandardServiceRegistryBuilder()
	                .configure("hibernate.cfg.xml")
	                .build();

	        sessionFactory = new MetadataSources( serviceRegistry )
	                    .buildMetadata()
	                    .buildSessionFactory();
	    }
	    return sessionFactory;
	}

	public static void main(String[] args) {
		//ArrayList<>
		try {
			session = getSessionFactory().openSession();
			//abrimos PRIMERA transaccion. Eso se hace siempre.
			session.beginTransaction();
			ArrayList<Client> c=new ArrayList<Client>();
			ArrayList<Comanda> com=new ArrayList<Comanda>();
			ArrayList<Lot> l=new ArrayList<Lot>();
			ArrayList<PeticioProveidor> pet=new ArrayList<PeticioProveidor>();
			ArrayList<Producte> p=new ArrayList<Producte>();
			ArrayList<Proveidor> pro=new ArrayList<Proveidor>();
			ArrayList<Address> ad=new ArrayList<Address>();
			
			for(int i=0;i<10;i++) {
				ad.add(new Address());
				pro.add(new Proveidor(ad.get(i)));
				c.add(new Client(ad.get(i)));
				p.add(new Producte());
				com.add(new Comanda(c.get(i)));
				l.add(new Lot(p.get(i))); 
				pet.add(new PeticioProveidor(pro.get(i),p.get(i)));//No se ni como va hulio, probarlo eooooo
			}
			for(Comanda co:com) {
				co.putAllProducte(p);
			}
			
			for(int i=0;i<10;i++) {
				session.saveOrUpdate(ad.get(i));
				session.saveOrUpdate(c.get(i));
				session.saveOrUpdate(com.get(i));
				session.saveOrUpdate(l.get(i));
				session.saveOrUpdate(pet.get(i));
				session.saveOrUpdate(p.get(i));
				session.saveOrUpdate(pro.get(i));
			}
	        session.getTransaction().commit();
			System.out.println("todo ha salido a pedir de Milhouse");
		} catch(Exception sqlException) {
			sqlException.printStackTrace();
			if(null != session.getTransaction()) {
				System.out.println("\n.......Transaction Is Being Rolled Back.......");
				session.getTransaction().rollback();
			}
			sqlException.printStackTrace();
		} finally {
			if(session != null) {
				session.close();
			}
		}
	}

}
