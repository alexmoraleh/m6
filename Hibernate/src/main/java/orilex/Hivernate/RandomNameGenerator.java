package orilex.Hivernate;

import java.util.Random;

public class RandomNameGenerator {
	private Random rnd=new Random();
	private String[] firstName =  new String[] { "Adam", "Alex", "Aaron", "Ben", "Carl", "Dan", "David", "Edward", "Fred", "Frank", "George", "Hal", "Hank", "Ike", "John", "Jack", "Joe", "Larry", "Monte", "Matthew", "Mark", "Nathan", "Otto", "Paul", "Peter", "Roger", "Roger", "Steve", "Thomas", "Tim", "Ty", "Victor", "Walter"};   
	private String[] lastName = new String[] { "Anderson", "Ashwoon", "Aikin", "Bateman", "Bongard", "Bowers", "Boyd", "Cannon", "Cast", "Deitz", "Dewalt", "Ebner", "Frick", "Hancock", "Haworth", "Hesch", "Hoffman", "Kassing", "Knutson", "Lawless", "Lawicki", "Mccord", "McCormack", "Miller", "Myers", "Nugent", "Ortiz", "Orwig", "Ory", "Paiser", "Pak", "Pettigrew", "Quinn", "Quizoz", "Ramachandran", "Resnick", "Sagar", "Schickowski", "Schiebel", "Sellon", "Severson", "Shaffer", "Solberg", "Soloman", "Sonderling", "Soukup", "Soulis", "Stahl", "Sweeney", "Tandy", "Trebil", "Trusela", "Trussel", "Turco", "Uddin", "Uflan", "Ulrich", "Upson", "Vader", "Vail", "Valente", "Van Zandt", "Vanderpoel", "Ventotla", "Vogal", "Wagle", "Wagner", "Wakefield", "Weinstein", "Weiss", "Woo", "Yang", "Yates", "Yocum", "Zeaser", "Zeller", "Ziegler", "Bauer", "Baxster", "Casal", "Cataldi", "Caswell", "Celedon", "Chambers", "Chapman", "Christensen", "Darnell", "Davidson", "Davis", "DeLorenzo", "Dinkins", "Doran", "Dugelman", "Dugan", "Duffman", "Eastman", "Ferro", "Ferry", "Fletcher", "Fietzer", "Hylan", "Hydinger", "Illingsworth", "Ingram", "Irwin", "Jagtap", "Jenson", "Johnson", "Johnsen", "Jones", "Jurgenson", "Kalleg", "Kaskel", "Keller", "Leisinger", "LePage", "Lewis", "Linde", "Lulloff", "Maki", "Martin", "McGinnis", "Mills", "Moody", "Moore", "Napier", "Nelson", "Norquist", "Nuttle", "Olson", "Ostrander", "Reamer", "Reardon", "Reyes", "Rice", "Ripka", "Roberts", "Rogers", "Root", "Sandstrom", "Sawyer", "Schlicht", "Schmitt", "Schwager", "Schutz", "Schuster", "Tapia", "Thompson", "Tiernan", "Tisler" };
	private String[] calle=new String[] {"Esta lin","Tarragona","Barcelona","Marco Polo","Gregorio","Unicef","Francesc +ia","Arturito +","Pepe Botella"};
	private String[] pob=new String[] {"New York","Tarrassa","Can Anglada","Castellar del Valles","Sabadell","Murcia","Oslo"};
	private String[] pais=new String[] {"Espa�a","USA","Catalu�a xd no","Francia","Colombia"};
	private String[] prod=new String[] {"Chocolate", "Manzana","Hojaldre","Frutos del bosque","Limon"};
	private String[] proo=new String[] {"OTravel","Orilex","Bar Juanito","Rey D�ner Kebab","Traster","Genius bar","El pulpito","Mediamarkt","Zara","Ferreteria Pepe","Pedro el sastre","la Caixa","NASA","Mi madre","Wikipedia","Pornhub","Josema","Marco David Albaricoque","Skereeeee"};
	public String GenerateName(){
		String[] results = new String[2];
		results[0] = firstName[rnd.nextInt(firstName.length)];
		results[1] = lastName[rnd.nextInt(lastName.length)];
		return (results[0]+" "+results[1]);
	}
	public String GenerateStreet() {
		return calle[rnd.nextInt(calle.length)];
	}
	public String GenerateVillage() {
		return pob[rnd.nextInt(pob.length)];
	}
	public String GenerateCountry() {
		return pais[rnd.nextInt(pais.length)];
	}
	public String GenerateProd() {
		return prod[rnd.nextInt(prod.length)];
	}
	public String GenerateProo() {
		return proo[rnd.nextInt(proo.length)];
	}
}

