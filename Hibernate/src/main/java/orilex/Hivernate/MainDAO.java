package orilex.Hivernate;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

import orilex.dao.*;

public class MainDAO {
	
	static Scanner reader = new Scanner(System.in);
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ComandaDao cd = new ComandaDao();
		LotDao ld = new LotDao();
		ClientDao cld = new ClientDao();
		ProducteDao pd = new ProducteDao();
		List<Producte> lp = new ArrayList<Producte>(); 
		lp = pd.list();
		
		//saveOrUpdate , list , get && delete
		
		Comanda c = new Comanda();
		
		List<Client> lc = new ArrayList<Client>();
		lc = cld.list();
		int i = 0;
		for(Client cl:lc) {
			System.out.println(cl.getNomClient() + " " + i);
			i++;
		}
		c.setCIF(lc.get(reader.nextInt()));
		c.setDataComanda(new Date());
		c.setDataLliurament(new Date());
		c.setEstat(ComandaEstat.PENDENT);
		
		int men = 0;
		Set<Producte> st = new HashSet<Producte>();
		while(men != 2) {
			men = reader.nextInt();
			switch(men) {
			case 1:
				i = 0;
				for(Producte p: lp) {
					System.out.println(p.getNomProducte() + " " + i);
					i++;
				}
				st.add(lp.get(reader.nextInt()));
				break;
			case 2:
				break;
			default:
				System.out.println("not valid.");
				break;
			}
		}
		c.setProductes(st);
		cd.saveOrUpdate(c);
		
		
		Lot l = new Lot();
		l.setDataCaducitat(new Date());
		l.setDataEntrada(new Date());
		
		i = 0;
		for(Producte p: lp) {
			System.out.println(p.getNomProducte() + " " + i);
			i++;
		}
		
		l.setProductes(lp.get(reader.nextInt()));
		l.setQuantitat(reader.nextInt());
		
		Producte ptr = pd.get(l.getProductes().getCodiProducte());
		ptr.setStock(ptr.getStock() + l.getQuantitat());
		pd.saveOrUpdate(ptr);
		
		
		
		ld.saveOrUpdate(l);
		
		//delete
		
		List<Comanda> lcs = cd.list();
		i = 0;
		for(Comanda cm:lcs) {
			System.out.println(cm.getIdComanda() + " " + i);
			i++;
		}
		
		cd.delete(lcs.get(0).getIdComanda());	
	}
}
