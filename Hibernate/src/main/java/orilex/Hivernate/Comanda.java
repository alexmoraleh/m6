package orilex.Hivernate;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="Comanda")
public class Comanda {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idComanda")
	protected int idComanda;
	@Column
	protected Date dataComanda;
	@Column
	protected Date dataLliurament;   
	@Column
	protected ComandaEstat estat;	//PENDENT - PREPARAT - TRANSPORT - LLIURAT
	
	@ManyToMany(cascade = {CascadeType.REFRESH})
    //Clave:  @JoinTable(name="nombreDeLaTablaIntermedia", joinColumns={@JoinColumn(name="LlavePrimariaThisEnTablaIntermedia")}, inverseJoinColumns={@JoinColumn(name="LlavePrimariaOtraClaseEnTablaIntermedia")})
    @JoinTable(name="ComandaProducte", joinColumns={@JoinColumn(name="idComanda")}, inverseJoinColumns={@JoinColumn(name="codiProducte")})
	Set<Producte> productes = new HashSet<Producte>();
	
	public Set<Producte> getProductes() {
		return productes;
	}
	public void setProductes(Set<Producte> productes) {
		this.productes = productes;
	}
	public Client getClient() {
		return client;
	}
	public void setClient(Client client) {
		this.client = client;
	}
	
	@ManyToOne
	@JoinColumn(name="Client")
	protected Client client;
	
	//relacio n a n amb producte
	//relacio n a 1 amb client

	
	
	public int getIdComanda() {
		return idComanda;
	}
	public Comanda() {
		Random rnd=new Random();
		this.dataComanda = (Date) new java.util.Date();
		this.dataLliurament = new Date(rnd.nextInt(2020)+2018, rnd.nextInt(12)+1, rnd.nextInt(30)+1);
		this.estat = ComandaEstat.values()[rnd.nextInt(4)];
	}
	public Comanda(Client c) {
		this();
		client=c;
	}
	public void setIdComanda(int idComanda) {
		this.idComanda = idComanda;
	}
	public Client getCIF() {
		return client;
	}
	public void setCIF(Client cIF) {
		client = cIF;
	}
	public Date getDataComanda() {
		return dataComanda;
	}
	public void setDataComanda(Date dataComanda) {
		this.dataComanda = dataComanda;
	}
	public Date getDataLliurament() {
		return dataLliurament;
	}
	public void setDataLliurament(Date dataLliurament) {
		this.dataLliurament = dataLliurament;
	}
	public ComandaEstat getEstat() {
		return estat;
	}
	public void setEstat(ComandaEstat estat) {
		this.estat = estat;
	}
	public void putProducte(Producte p) {
		productes.add(p);
	}
	public void putAllProducte(ArrayList<Producte> p) {
		productes.addAll(p);
	}
	
}
