package orilex.Hivernate;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import javax.persistence.*;

@Entity
@Table(name = "Producte")
public class Producte{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(updatable = false, nullable = false)
	protected int codiProducte;
	
	@Column
	protected String nomProducte;
	
	@Column
	protected int stock;
	
	@Column
	protected int stockMinim;
	
	@Column
	protected UnitatMesura unitat;
	
	@Column
	protected Tipus tipus;
	
	@Column
	protected double preuVenda;
	
	//relacio 1 a n amb lot
	@OneToMany(mappedBy="producte")
	protected Set<Lot> lots = new HashSet<Lot>();
	
	//relacio n a n amb ell mateix
	@ManyToMany(cascade = {CascadeType.REFRESH})
	@JoinTable(name="ProducteProducte", joinColumns= {@JoinColumn(name="codiCompost")}, inverseJoinColumns= {@JoinColumn(name="codiComponent")})
	protected Set<Producte> productes = new HashSet<Producte>();
	
	//relacio 1 a n amb peticionsproveidor
	@OneToMany(mappedBy="producte")
	protected Set<PeticioProveidor> peticio = new HashSet<PeticioProveidor>();
	
	//relacio n a n amb comanda
	//@ManyToMany(cascade = {CascadeType.REFRESH}, mappedBy="Producte")
	//protected Set<Comanda> peticions = new HashSet<Comanda>();
	
	public Producte() {
		 Random rnd=new Random();
		nomProducte=new RandomNameGenerator().GenerateProd();
		stock=rnd.nextInt(800);
		stockMinim=rnd.nextInt(50);
		switch(rnd.nextInt(3)) {
		case 0:
			unitat=UnitatMesura.GRAMS;
			break;
		case 1:
			unitat=UnitatMesura.LLITRE;
			break;
		case 2:
			unitat=UnitatMesura.UNITAT;
			break;
		}
		switch(rnd.nextInt(2)) {
		case 0:
			tipus=Tipus.INGREDIENT;
			break;
		case 1:
			tipus=Tipus.VENDIBLE;
			break;
		}
		preuVenda=rnd.nextDouble();
	}

	public int getCodiProducte() {
		return codiProducte;
	}

	public void setCodiProducte(int codiProducte) {
		this.codiProducte = codiProducte;
	}

	public String getNomProducte() {
		return nomProducte;
	}

	public void setNomProducte(String nomProducte) {
		this.nomProducte = nomProducte;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	public int getStockMinim() {
		return stockMinim;
	}

	public void setStockMinim(int stockMinim) {
		this.stockMinim = stockMinim;
	}

	public UnitatMesura getUnitat() {
		return unitat;
	}

	public void setUnitat(UnitatMesura unitat) {
		this.unitat = unitat;
	}

	public Tipus getTipus() {
		return tipus;
	}

	public void setTipus(Tipus tipus) {
		this.tipus = tipus;
	}

	public double getPreuVenda() {
		return preuVenda;
	}

	public void setPreuVenda(double preuVenda) {
		this.preuVenda = preuVenda;
	}

	public Set<Producte> getProductes() {
		return productes;
	}

	public void setProductes(Set<Producte> productes) {
		this.productes = productes;
	}

	public Set<PeticioProveidor> getPeticio() {
		return peticio;
	}

	public void setPeticio(Set<PeticioProveidor> peticio) {
		this.peticio = peticio;
	}

	/*public Set<Comanda> getPeticions() {
		return peticions;
	}

	public void setPeticions(Set<Comanda> peticions) {
		this.peticions = peticions;
	}*/
	
	
}