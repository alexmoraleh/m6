package orilex.Hivernate;

import java.util.Date;
import java.util.Random;

import javax.persistence.*;

@Entity
@Table(name = "PeticioProveidor")
public class PeticioProveidor {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	protected int idPeticio;
	
	@Column
	protected int tipusPeticio;
	
	@Column
	protected Date dataOrdre;
	
	@Column
	protected Date dataRecepcio;   
	
	@Column
	protected ComandaEstat estat;
	
	public int getIdPeticio() {
		return idPeticio;
	}
	public void setIdPeticio(int idPeticio) {
		this.idPeticio = idPeticio;
	}
	public int getTipusPeticio() {
		return tipusPeticio;
	}
	public void setTipusPeticio(int tipusPeticio) {
		this.tipusPeticio = tipusPeticio;
	}
	public Date getDataOrdre() {
		return dataOrdre;
	}
	public void setDataOrdre(Date dataOrdre) {
		this.dataOrdre = dataOrdre;
	}
	public Date getDataRecepcio() {
		return dataRecepcio;
	}
	public void setDataRecepcio(Date dataRecepcio) {
		this.dataRecepcio = dataRecepcio;
	}
	public ComandaEstat getEstat() {
		return estat;
	}
	public void setEstat(ComandaEstat estat) {
		this.estat = estat;
	}
	public Proveidor getProveidor() {
		return proveidor;
	}
	public void setProveidor(Proveidor proveidor) {
		this.proveidor = proveidor;
	}
	public Producte getProducte() {
		return producte;
	}
	public void setProducte(Producte producte) {
		this.producte = producte;
	}
	//relacio n a 1 amb proveidor
	@ManyToOne(cascade = {CascadeType.REFRESH})
	@JoinColumn(name="Proveidor")
	protected Proveidor proveidor;
	
	//relacio n a 1 amb producte
	@ManyToOne(cascade = {CascadeType.REFRESH})
	@JoinColumn(name="Producte")
	protected Producte producte;
	
	/* xd */
	public PeticioProveidor() {
		Random rnd=new Random();
		// TODO Auto-generated constructor stub
		tipusPeticio=rnd.nextInt(50);
		dataOrdre=new Date(rnd.nextInt(2017)+2016, rnd.nextInt(12)+1, rnd.nextInt(30)+1);
		dataRecepcio= new Date();
		estat=ComandaEstat.values()[rnd.nextInt(4)];
	}
	PeticioProveidor(Proveidor p, Producte pr) {
		this();
		proveidor=p;
		producte=pr;
	}
}
