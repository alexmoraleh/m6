package orilex.Hivernate;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import javax.persistence.*;

@Entity
@Table(name = "Proveidor")
public class Proveidor{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(updatable = false, nullable = false)
	protected int idProveidor;
	
	@Column
	protected String nomProveidor;
	
	@Column
	protected String CIF;
	
	@Column
	protected boolean actiu;
	
	@Column
	protected String personaContacte;
	
	//relacio 1 a 1 amb address
	@OneToOne(cascade= {CascadeType.PERSIST})
	@JoinColumn(name="numero")
	protected Address numero;
	
	//relacio 1 a n amb peticioproveidor
	@JoinColumn(name="PeticioProveidor")
	@OneToMany(cascade = {CascadeType.REFRESH})
	private Set<PeticioProveidor> peticions=new HashSet<PeticioProveidor>();
	
	/* xd */
	Proveidor(){
		Random rnd=new Random();
		nomProveidor=new RandomNameGenerator().GenerateProo();
		CIF=Double.toString(rnd.nextDouble());//Acabar constructores
		actiu=true;
		personaContacte=new RandomNameGenerator().GenerateName();
	}
	Proveidor(Address a){
		this();
		numero=a;
	}
	public int getIdProveidor() {
		return idProveidor;
	}
	public void setIdProveidor(int idProveidor) {
		this.idProveidor = idProveidor;
	}
	public String getNomProveidor() {
		return nomProveidor;
	}
	public void setNomProveidor(String nomProveidor) {
		this.nomProveidor = nomProveidor;
	}
	public String getCIF() {
		return CIF;
	}
	public void setCIF(String cIF) {
		CIF = cIF;
	}
	public boolean isActiu() {
		return actiu;
	}
	public void setActiu(boolean actiu) {
		this.actiu = actiu;
	}
	public String getPersonaContacte() {
		return personaContacte;
	}
	public void setPersonaContacte(String personaContacte) {
		this.personaContacte = personaContacte;
	}
	public Address getNumero() {
		return numero;
	}
	public void setNumero(Address numero) {
		this.numero = numero;
	}
	public Set<PeticioProveidor> getPeticions() {
		return peticions;
	}
	public void setPeticions(Set<PeticioProveidor> peticions) {
		this.peticions = peticions;
	}
}